export class UnidadeSelected {

        id: number
        nome_fantasia: string
        endereco: string
        numero: string
        bairro: string
        cidade: string
        saldo: number

}