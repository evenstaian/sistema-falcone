import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'esconderCelular'
})
export class EsconderCelular implements PipeTransform{
    transform(texto: string): string {
        if(texto.length>7){
            return '*******' + texto.substr(7,10) 
        }
        return texto
    }
}