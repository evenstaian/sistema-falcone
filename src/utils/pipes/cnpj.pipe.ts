import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'cnpjPipe'
})
export class CnpjPipe implements PipeTransform{
    transform(texto: string): string {
        if(texto.length===14){
            return texto.substr(0,2) 
            + '.' + texto.substr(2,3)
            + '.' + texto.substr(5,3)
            + '/' + texto.substr(8,4)
            + '-' + texto.substr(12,2)
        }
        return texto
    }
}