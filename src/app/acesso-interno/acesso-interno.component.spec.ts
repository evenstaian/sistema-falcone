import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcessoInternoComponent } from './acesso-interno.component';

describe('AcessoInternoComponent', () => {
  let component: AcessoInternoComponent;
  let fixture: ComponentFixture<AcessoInternoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcessoInternoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcessoInternoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
