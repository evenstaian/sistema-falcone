import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperadorescadastradosComponent } from './operadorescadastrados.component';

describe('OperadorescadastradosComponent', () => {
  let component: OperadorescadastradosComponent;
  let fixture: ComponentFixture<OperadorescadastradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperadorescadastradosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperadorescadastradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
