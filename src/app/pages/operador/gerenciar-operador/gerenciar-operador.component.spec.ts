import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciarOperadorComponent } from './gerenciar-operador.component';

describe('GerenciarOperadorComponent', () => {
  let component: GerenciarOperadorComponent;
  let fixture: ComponentFixture<GerenciarOperadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciarOperadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciarOperadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
