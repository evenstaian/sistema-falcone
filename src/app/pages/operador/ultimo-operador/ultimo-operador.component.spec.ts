import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UltimoOperadorComponent } from './ultimo-operador.component';

describe('UltimoOperadorComponent', () => {
  let component: UltimoOperadorComponent;
  let fixture: ComponentFixture<UltimoOperadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UltimoOperadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UltimoOperadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
