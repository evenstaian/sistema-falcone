import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeusDadosColabComponent } from './meus-dados-colab.component';

describe('MeusDadosColabComponent', () => {
  let component: MeusDadosColabComponent;
  let fixture: ComponentFixture<MeusDadosColabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeusDadosColabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeusDadosColabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
