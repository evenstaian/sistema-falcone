import { ValidaCpf } from 'src/utils/valida-cpf';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/services/requests.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';

@Component({
  selector: 'app-meus-dados-colab',
  templateUrl: './meus-dados-colab.component.html',
  styleUrls: ['./meus-dados-colab.component.css'],
  providers: [ 
    ValidaCpf,
    ResponsesModuleService ]
})
export class MeusDadosColabComponent implements OnInit {
  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]

  public responseScreen : boolean = false
  public estadoLoad : boolean = false

  public mensagem_loader : string = ""

  public formMeusDados :FormGroup = new FormGroup({
    'nome' : new FormControl(null),
    'cpf' : new FormControl(null),
    'email' : new FormControl(null)
  });

  public nomeValido : boolean = false
  public cpfValido : boolean = false
  public emailValido : boolean = false

  public estadoForm : string = "disabled"

  constructor(
    private validaCpf : ValidaCpf,
    private requests : Requests,
    private responsesModuleService : ResponsesModuleService) { }

  ngOnInit() {
    this.setInfo()
  }

  setInfo() {
    var dados_conta = JSON.parse(localStorage.getItem('dados_conta'))
    var colaborador = dados_conta.usuario

    this.formMeusDados.get('nome').setValue(colaborador[0].nome)
    this.formMeusDados.get('cpf').setValue(colaborador[0].cpf)
    this.formMeusDados.get('email').setValue(colaborador[0].email)
    
    this.validaNome("")
    this.validaCPF(this.formMeusDados.get('cpf'))
    this.validaEmail("")

    this.estadoForm = "disabled"
  }

  alterarDados(){
    let dados_pessoais: any = {
      'nome' : this.formMeusDados.value.nome,
      'email' : this.formMeusDados.value.email
    }

    this.requests.atualizarDadosPessoaisColaborador(dados_pessoais).subscribe(
      res => {
        var resp_dados : any = res
        if(resp_dados.code===200){
          this.putInStorage(resp_dados.data);
          this.setInfo();
        }else{

        }
      }
    )
  }

  putInStorage(dados_conta){
    localStorage.setItem('dados_conta', JSON.stringify(dados_conta))
  }

  validaNome(nome : string){
    if(this.formMeusDados.value.nome.length<7 || this.formMeusDados.value.nome.indexOf(" ") == -1){
      this.nomeValido = false  
    }else{
      this.nomeValido = true
    }
    this.ativarBotao()
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(this.formMeusDados.value.cpf)
    this.ativarBotao()
  }

  validaEmail(email_teste: string){
    if(this.formMeusDados.value.email.indexOf('@') == -1 || this.formMeusDados.value.email.indexOf('.') == -1 ){
      this.emailValido = false
    }else{
      this.emailValido = true
    }
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido && this.cpfValido && this.emailValido){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

}
