import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { IMyDpOptions } from 'mydatepicker';
import { HttpClient } from '@angular/common/http';
import { InserviceService } from '../../../services/inservice.service'
import { Subject } from 'rxjs';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Requests } from 'src/services/requests.service';
declare var $;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [
    trigger('graficoAnim', [
      state('escondido', style({
        opacity: 0
      })),
        
      state('visivel', style({
        opacity: 1
      })),
      transition('escondido => visivel',[
        style({opacity: 0, transform:'translate(0px, 100px)'}),
        animate('0.5s ease-in-out')
      ]),
      transition('visivel => escondido',[
        style({opacity: 0, transform:'translate(0px, -100px)'}),
        animate('0.5s ease-in-out')
      ]),
    ])
  ]
})
export class DashboardComponent implements OnInit {

alldados: any = []
dtOptions: DataTables.Settings = {}
dtTrigger: Subject<any> = new Subject()
  ativo1: boolean = false
  ativo2: boolean = true
  chart = []
  chart1 = []
  chart2 = []
  nomes = ''
  info : any
  info1 : any
  eounao: number
  gtroco1 : any
  pediodo1 : any
  ranking1 : any
  total1 : any
  juntatroco1 : any
  juntadias1 : any
  juntavalor1 : any
  juntaqnt1 : any
  juntaval1 : any
  troco_atual1 : any
  troco_passado1 : any
  troco_depositado1 : any
  num_troco : any
  valor_troco : any
  html: any
  html1: any
  html2: any
  


  ranking_unidades : boolean = false
  ranking_unidades_list : any

  estadoLoad : boolean = true
  estadoGrafico : string = "escondido"

  colaborador :any
  funcionalidade :any
  is_admin :number
  id_unidade_colaborador :number


   // @ViewChild('dataTable') table;
   // dataTable: any;

  constructor(
      private http: HttpClient, 
      private serv: InserviceService,
      private requests : Requests) { 

   }

public inicio : any = {  };
public fim : any = {  };

  public model1: any = {  };
  public model2: any = {  };
  public model3: any = {  };

  public myDatePickerOptions1: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
  }

  public myDatePickerOptions2: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
  }

    

  ngOnInit() :void {
      
    //DADOS DA CONTA
    this.buscardadosunidade();

    this.buscauto(); 

    this.buscarAutoRankingUnidades();
    
}


  ///////////////////////////////////////////////////////////////////////////////////////////////
  public buscauto(){
    this.html = '<canvas id="lineChart" width="400" height="400"></canvas>'
    document.getElementById('grafico').innerHTML = this.html
    this.html1 = '<canvas id="pizzaChart1" width="400" height="400"></canvas>'
    document.getElementById('graficop1').innerHTML = this.html1
    this.html2 = '<canvas id="pizzaChart2" width="400" height="400"></canvas>'
    document.getElementById('graficop2').innerHTML = this.html2
      
    var dados_conta = JSON.parse(localStorage.getItem('dados_conta'))
    this.colaborador = dados_conta.colaborador
    this.funcionalidade = dados_conta.funcionalidades

    this.id_unidade_colaborador = this.colaborador[0].id_unidade
    this.is_admin = this.colaborador[0].admin

    

    if(this.is_admin !== 0){
        //  console.log(this.id_unidade_colaborador)
        //  console.log('------')
        //  console.log(this.is_admin)
        this.eounao = this.id_unidade_colaborador
    }else{
        this.eounao = 0;
    }

    var hoje = new Date()
    const dia = hoje.getDate()
    const mes = hoje.getMonth() + 1
    const ano = hoje.getFullYear()
    const minuto = hoje.getMinutes();
    const hora = hoje.getHours();
    const segundo = hoje.getSeconds();
    var junta = `${ano}-${mes}-${dia} ${hora}:${minuto}:${segundo}`
    this.fim = { date: {
        year: ano,
        month: mes,
        day: dia
    }}


    var ago = new Date()
    ago.setDate(ago.getDate() - 30);
    var diatras = ago
    const dia1 = diatras.getDate()
    const mes1 = diatras.getMonth() + 1
    const ano1 = diatras.getFullYear()
    const minuto1 = diatras.getMinutes();
    const hora1 = diatras.getHours();
    const segundo1 = diatras.getSeconds();
    var junta1 = `${ano1}-${mes1}-${dia1} ${hora1}:${minuto1}:${segundo1}`
    this.inicio = { date: {
        year: ano1,
        month: mes1,
        day: dia1
    }}


    

    //this.dataTable = $(this.table.nativeElement);
    //this.dataTable.dataTable();

    
    this.serv.buscauto(junta1, junta, this.eounao).subscribe(dados => {
    
        this.gtroco1 = dados.data.grafico
        this.info = dados.data.info_geral
        this.pediodo1 = dados.data.periodo
        this.ranking1 = dados.data.ranking
        this.total1 = dados.data.totais
  
      
  
  
          //informacao set
          this.info1 = dados.data.info_geral
          //console.log(this.info1)
          this.troco_atual1 = this.info1[0].saldo_atual
          this.troco_passado1 =  this.info1[0].trocos_passados
          this.troco_depositado1 =  this.info1[0].saldo_acumulada
          if(this.troco_atual1 === null){
              this.troco_atual1 = "0,00"
          }
          if(this.troco_passado1 === null){
              this.troco_passado1 = "0,00"
          }
          if(this.troco_depositado1 === null){
              this.troco_depositado1 = "0,00"
          }
          //fim informacao
  
          this.pediodo1 = dados.data.periodo
  
          //////////Ranking datatable/////////////////////////////////
          this.ranking1 = dados.data.ranking
         // console.log(this.ranking1)
          //////////Ranking datatable/////////////////////////////////
  
  
          //totais aqui////////////////////////
          this.num_troco = dados.data.totais[0].num_troco
        //  console.log(this.num_troco)
          this.valor_troco = dados.data.totais[0].valor_troco
          if(this.num_troco === null || this.num_troco === 0){
              this.num_troco = "0,00"
          }
          if(this.valor_troco === null || this.num_troco === 0){
              this.valor_troco = "0,00"
          }
          //totais aqui////////////////////////
  
        this.juntatroco1 = [];
        this.juntadias1 = [];
        this.juntavalor1 = [];
        this.juntaqnt1 = [];
        this.juntaval1 = [];
          for(let i=0; i<this.gtroco1.length; i++){
              
              this.juntatroco1.push(this.gtroco1[i].num_troco)
              this.juntavalor1.push(this.gtroco1[i].valor_troco)
              this.juntadias1.push(this.gtroco1[i].dia+'/'+this.gtroco1[i].mes+'/'+this.gtroco1[i].ano)
              
          }
  
          for(let j=0; j<this.pediodo1.length; j++){
              this.juntaqnt1.push(this.pediodo1[j].num_troco)
              this.juntaval1.push(this.pediodo1[j].valor_troco)
          }
          //console.log(juntaqnt)
          
          this.html = '<canvas id="lineBusca" width="400" height="400"></canvas>'
          document.getElementById('grafico').innerHTML = this.html
          this.html1 = '<canvas id="pizzaBusca1" width="400" height="400"></canvas>'
          document.getElementById('graficop1').innerHTML = this.html1
          this.html2 = '<canvas id="pizzaBusca2" width="400" height="400"></canvas>'
          document.getElementById('graficop2').innerHTML = this.html2
  
          this.chart = new Chart('lineBusca',{
              type: 'line',
              data: {
                  labels: this.juntadias1,
                  datasets: [{
                      label: 'Quantidade de Troco',
                      data: this.juntatroco1,
                      backgroundColor: [
                          'rgba(255,255,255,0.1)',
                          'rgba(255, 255, 235, 0.8)',
                          'rgba(255, 255, 235, 0.8)',
                          'rgba(255, 255, 235, 0.8)',
                          'rgba(255, 255, 235, 0.8)',
                          'rgba(255, 255, 235, 0.8)'
                      ],
                      borderColor: [
                          'rgba(255,255,255,1)',
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 206, 86, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(255, 159, 64, 1)'
                      ],
                      borderWidth: 2
                  },
                  {
                      label: 'Valor de Troco',
                      data: this.juntavalor1,
                      backgroundColor: [
                          'rgba(232,49,192,0.1)',
                          'rgba(232,49,192, 0.8)',
                          'rgba(232,49,192, 0.8)',
                          'rgba(232,49,192, 0.8)',
                          'rgba(232,49,192, 0.8)',
                          'rgba(232,49,192, 0.8)'
                      ],
                      borderColor: [
                          'rgba(232,49,192,1)',
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 206, 86, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(255, 159, 64, 1)'
                      ],
                      borderWidth: 2
                  }
              
                  ]
              },
              options: {
                  responsive: true,
                  maintainAspectRatio: false,
                  legend:{
                      display: false
                  },
                title:{
                  text: "Line Chart",
                  display:false
                },
                  scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero:true
                          }
                      }]
                  }
              }
              
          });
          
          
  
  
  
         this.chart1 = new Chart('pizzaBusca1',{
              type: 'pie',
              data : {
                  datasets: [{
                      data:this.juntaqnt1,
                      backgroundColor: [
                          '#6562a2',
                          '#61c3d6',
                          '#309fd6'
                      ]
                  }],
              
                  // These labels appear in the legend and in the tooltips when hovering different arcs
                  labels: [
                      'MANHÃ',
                      'TARDE',
                      'NOITE'
                  ]
              },
              options: {
                  legend:{
                      position: 'bottom'
                  }
                  
              }
          });
      
          
      
      
      
          this.chart2 = new Chart('pizzaBusca2',{
              type: 'pie',
              data : {
                  datasets: [{
                      data: this.juntaval1,
                      backgroundColor: [
                          '#b7a9d3',
                          '#e71860',
                          '#d373ac'
                      ]
                  }],
              
                  labels: [
                      'MANHÃ',
                      'TARDE',
                      'NOITE'
                  ]
              },
              options: {
                  legend:{
                      position: 'bottom'
                  }
              }
          });
      

    })
    

    this.chart = new Chart('lineChart',{
        type: 'line',
        data: {
            labels: this.juntadias1,
            datasets: [{
                label: '# of Votes',
                data: this.juntatroco1,
                backgroundColor: [
                    'rgba(255,255,255,0.1)',
                    'rgba(255, 255, 235, 0.8)',
                    'rgba(255, 255, 235, 0.8)',
                    'rgba(255, 255, 235, 0.8)',
                    'rgba(255, 255, 235, 0.8)',
                    'rgba(255, 255, 235, 0.8)'
                ],
                borderColor: [
                    'rgba(255,255,255,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 2
            },
            {
                label: '# of Votes',
                data: this.juntavalor1,
                backgroundColor: [
                    'rgba(232,49,192,0.1)',
                    'rgba(232,49,192, 0.8)',
                    'rgba(232,49,192, 0.8)',
                    'rgba(232,49,192, 0.8)',
                    'rgba(232,49,192, 0.8)',
                    'rgba(232,49,192, 0.8)'
                ],
                borderColor: [
                    'rgba(232,49,192,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 2
            }
        
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend:{
                display: false
            },
            title:{
            text: "Line Chart",
            display:false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    

    this.chart1 = new Chart('pizzaChart1',{
        type: 'pie',
        data : {
            datasets: [{
                data: this.juntaqnt1,
                backgroundColor: [
                    '#6562a2',
                    '#61c3d6',
                    '#309fd6'
                ]
            }],
        
            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'MANHÃ',
                'TARDE',
                'NOITE'
            ]
        },
        options: {
            legend:{
                position: 'bottom'
            }
            
        }
    });

    



    this.chart2 = new Chart('pizzaChart2',{
        type: 'pie',
        data : {
            datasets: [{
                data: this.juntaval1,
                backgroundColor: [
                    '#b7a9d3',
                    '#e71860',
                    '#d373ac'
                ]
            }],
        
            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'MANHÃ',
                'TARDE',
                'NOITE'
            ]
        },
        options: {
            legend:{
                position: 'bottom'
            }
        }
    });

    
}  

  /////////////////////////////////////////////////////////////////////////////////////////////////









  public buscardadosunidade(): void {
    //console.log(this.formulario.value.cnpj, this.formulario.value.senha)
  	this.serv.unidades().subscribe(dados => {

        this.mostrarLoader(false, "visivel")
          
          if(dados.code === 200){
              this.nomes = dados.data
          }
      
  	})
  }

  
    

  buscaLojas(form){
    this.mostrarLoader(true, "escondido")

    this.html = ''
    document.getElementById('grafico').innerHTML = this.html
    this.html1 = ''
    document.getElementById('graficop1').innerHTML = this.html1
    this.html2 = ''
    document.getElementById('graficop2').innerHTML = this.html2
    
    this.ativo1 = true
    this.ativo2 = false
    let d1;
    let d2;
    let loja = form.value.loja
    
    if(form.value.data_inicio.formatted !== undefined){
     d1 = form.value.data_inicio.formatted;
    }else{
     d1 = '';
    }
    if(form.value.data_fim.formatted !== undefined){
     d2 = form.value.data_fim.formatted;
    }else{
     d2 = '';
    }
    let separa1 = d1.split("/")
    let separa2 = d2.split("/")
   // console.log(d2)
    let ndata1 = separa1[2]+'-'+separa1[1]+'-'+separa1[0] + ' 00:00:00'
    let ndata2 = separa2[2]+'-'+separa2[1]+'-'+separa2[0] + ' 23:59:59'
    

    this.serv.buscarLojas(ndata1, ndata2, loja).subscribe(dados => {
     
    this.mostrarLoader(false, "visivel")

      this.gtroco1 = dados.data.grafico
      this.info = dados.data.info_geral
      this.pediodo1 = dados.data.periodo
      this.ranking1 = dados.data.ranking
      this.total1 = dados.data.totais

    


        //informacao set
        this.info1 = dados.data.info_geral
        //console.log(this.info1)
        this.troco_atual1 = this.info1[0].saldo_atual
        this.troco_passado1 =  this.info1[0].trocos_passados
        this.troco_depositado1 =  this.info1[0].saldo_acumulada
        if(this.troco_atual1 === null){
            this.troco_atual1 = "0,00"
        }
        if(this.troco_passado1 === null){
            this.troco_passado1 = "0,00"
        }
        if(this.troco_depositado1 === null){
            this.troco_depositado1 = "0,00"
        }
        //fim informacao

        this.pediodo1 = dados.data.periodo

        //////////Ranking datatable/////////////////////////////////
        this.ranking1 = dados.data.ranking
       // console.log(this.ranking1)
        //////////Ranking datatable/////////////////////////////////


        //totais aqui////////////////////////
        this.num_troco = dados.data.totais[0].num_troco
      //  console.log(this.num_troco)
        this.valor_troco = dados.data.totais[0].valor_troco
        if(this.num_troco === null || this.num_troco === 0){
            this.num_troco = "0,00"
        }
        if(this.valor_troco === null || this.num_troco === 0){
            this.valor_troco = "0,00"
        }
        //totais aqui////////////////////////



      this.juntatroco1 = [];
      this.juntadias1 = [];
      this.juntavalor1 = [];
      this.juntaqnt1 = [];
      this.juntaval1 = [];
        for(let i=0; i<this.gtroco1.length; i++){
            
            this.juntatroco1.push(this.gtroco1[i].num_troco)
            this.juntavalor1.push(this.gtroco1[i].valor_troco)
            this.juntadias1.push(this.gtroco1[i].dia+'/'+this.gtroco1[i].mes+'/'+this.gtroco1[i].ano)
            
        }

        for(let j=0; j<this.pediodo1.length; j++){
            this.juntaqnt1.push(this.pediodo1[j].num_troco)
            this.juntaval1.push(this.pediodo1[j].valor_troco)
        }
        //console.log(juntaqnt)
        
        this.html = '<canvas id="lineBusca" width="400" height="400"></canvas>'
        document.getElementById('grafico').innerHTML = this.html
        this.html1 = '<canvas id="pizzaBusca1" width="400" height="400"></canvas>'
        document.getElementById('graficop1').innerHTML = this.html1
        this.html2 = '<canvas id="pizzaBusca2" width="400" height="400"></canvas>'
        document.getElementById('graficop2').innerHTML = this.html2

        this.chart = new Chart('lineBusca',{
            type: 'line',
            data: {
                labels: this.juntadias1,
                datasets: [{
                    label: 'Quantidade de Troco',
                    data: this.juntatroco1,
                    backgroundColor: [
                        'rgba(255,255,255,0.1)',
                        'rgba(255, 255, 235, 0.8)',
                        'rgba(255, 255, 235, 0.8)',
                        'rgba(255, 255, 235, 0.8)',
                        'rgba(255, 255, 235, 0.8)',
                        'rgba(255, 255, 235, 0.8)'
                    ],
                    borderColor: [
                        'rgba(255,255,255,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 2
                },
                {
                    label: 'Valor de Troco',
                    data: this.juntavalor1,
                    backgroundColor: [
                        'rgba(232,49,192,0.1)',
                        'rgba(232,49,192, 0.8)',
                        'rgba(232,49,192, 0.8)',
                        'rgba(232,49,192, 0.8)',
                        'rgba(232,49,192, 0.8)',
                        'rgba(232,49,192, 0.8)'
                    ],
                    borderColor: [
                        'rgba(232,49,192,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 2
                }
            
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend:{
                    display: false
                },
              title:{
                text: "Line Chart",
                display:false
              },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
            
        });
        
        



       this.chart1 = new Chart('pizzaBusca1',{
            type: 'pie',
            data : {
                datasets: [{
                    data:this.juntaqnt1,
                    backgroundColor: [
                        '#6562a2',
                        '#61c3d6',
                        '#309fd6'
                    ]
                }],
            
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: [
                    'MANHÃ',
                    'TARDE',
                    'NOITE'
                ]
            },
            options: {
                legend:{
                    position: 'bottom'
                }
                
            }
        });
    
        
    
    
    
        this.chart2 = new Chart('pizzaBusca2',{
            type: 'pie',
            data : {
                datasets: [{
                    data: this.juntaval1,
                    backgroundColor: [
                        '#b7a9d3',
                        '#e71860',
                        '#d373ac'
                    ]
                }],
            
                labels: [
                    'MANHÃ',
                    'TARDE',
                    'NOITE'
                ]
            },
            options: {
                legend:{
                    position: 'bottom'
                }
            }
        });

        this.buscarRankingUnidades(ndata1, ndata2)

    })

  }

  buscarAutoRankingUnidades(){

    var hoje = new Date()
    const dia = hoje.getDate()
    const mes = hoje.getMonth() + 1
    const ano = hoje.getFullYear()
    const minuto = hoje.getMinutes();
    const hora = hoje.getHours();
    const segundo = hoje.getSeconds();
    var junta = `${ano}-${mes}-${dia} ${hora}:${minuto}:${segundo}`

    var ago = new Date()
    ago.setDate(ago.getDate() - 30);
    var diatras = ago
    const dia1 = diatras.getDate()
    const mes1 = diatras.getMonth() + 1
    const ano1 = diatras.getFullYear()
    const minuto1 = diatras.getMinutes();
    const hora1 = diatras.getHours();
    const segundo1 = diatras.getSeconds();
    var junta1 = `${ano1}-${mes1}-${dia1} ${hora1}:${minuto1}:${segundo1}`

    var dados_ranking : any = {
        'data_inicio' : junta1,
        'data_fim' : junta
    }

    /*this.requests.getRankingUnidades(dados_ranking).subscribe(
        res => {
            var resp_ranking : any = res
            if(resp_ranking.code===200){
                this.ranking_unidades = true
                this.ranking_unidades_list = resp_ranking.data
            }else{
                this.ranking_unidades = false
            }
        }
      )
      */
  }

  buscarRankingUnidades(inicio, fim){

    var dados_ranking : any = {
        'data_inicio' : inicio,
        'data_fim' : fim
    }
    /*
    this.requests.getRankingUnidades(dados_ranking).subscribe(
        res => {
            var resp_ranking : any = res

            if(resp_ranking.code===200){
                this.ranking_unidades = true
                this.ranking_unidades_list = resp_ranking.data
            }else{
                this.ranking_unidades = false
            }
        }
      )*/
  }

  mostrarLoader(status: boolean, status_grafico : string){
      this.estadoLoad = status
      this.estadoGrafico = status_grafico
  }

}
