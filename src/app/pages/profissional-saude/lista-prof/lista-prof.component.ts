import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/services/requests.service';
import { Constants } from 'src/utils/Constantes';
import { Router } from '@angular/router';
import { EditProfissionalService } from 'src/services/modules_services/edit_profissional.service';

@Component({
  selector: 'app-lista-prof',
  templateUrl: './lista-prof.component.html',
  styleUrls: ['./lista-prof.component.css'],
  providers: [ EditProfissionalService ]
})
export class ListaProfComponent implements OnInit {

  public estadoLoad : boolean = false
  public mensagem_loader : string = ""

  public url_img_prof : string = Constants.API_IMAGENS_PROFISSIONAIS

  public profissionais : any = [ ]

  constructor(
    private requests : Requests,
    private editProfissionalService : EditProfissionalService,
    private rotas : Router) { }

  ngOnInit() {
    this.listaProfissionais()
  }

  listaProfissionais(){
    this.mostrarLoader(true, "Carregando Profissionais")
    var dados_profissionais : any = {
      'limit' : 20,
      'offset' : 0
    }

    this.requests.listarProfissionais(dados_profissionais).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_profissionais : any = res

        if(resp_profissionais.code===200){
          this.profissionais = resp_profissionais.data
          console.log("PROFISSIONAIS DADOS", this.profissionais);
        }else{

        }
      }
    )
  }

  mostrarLoader(status: boolean, mensagem : string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarImg(img : String){
    if(img!=null){
      return this.url_img_prof+img
    }else{
      return "/assets/new_user_img.svg"
    }
  }

  editarProfissional(profissional){
    this.editProfissionalService.enviaEditProfissional(profissional)
    this.rotas.navigate(['dashboard/editar-prof'])
  }

}
