import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnviaImagemProfissionalComponent } from './envia-imagem-profissional.component';

describe('EnviaImagemProfissionalComponent', () => {
  let component: EnviaImagemProfissionalComponent;
  let fixture: ComponentFixture<EnviaImagemProfissionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnviaImagemProfissionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnviaImagemProfissionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
