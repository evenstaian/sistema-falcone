import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciarProfissionalComponent } from './gerenciar-profissional.component';

describe('GerenciarProfissionalComponent', () => {
  let component: GerenciarProfissionalComponent;
  let fixture: ComponentFixture<GerenciarProfissionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciarProfissionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciarProfissionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
