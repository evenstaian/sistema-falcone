import { Unidades } from './../../../models/unidade.model';
import { ValidaCnpj } from './../../../utils/valida-cnpj';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InserviceService } from '../../../services/inservice.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ErrorInputService } from 'src/services/modules_services/error_input.service';
import { Requests } from 'src/services/requests.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { EditUnidadeService } from 'src/services/modules_services/edit_unidade.service';
import { Router } from '@angular/router';
import { ImagemModuleService } from 'src/services/modules_services/enviar_imagem.service';
declare var $

//flag pra verificar o primeiro load da lista de unidades
var flag = 1;

@Component({
  selector: 'app-lojas',
  templateUrl: './lojas.component.html',
  styleUrls: ['./lojas.component.css'],
  providers: [ 
    ErrorInputService, 
    ResponsesModuleService, 
    ValidaCnpj,
    EditUnidadeService,
    ImagemModuleService ]
})
export class LojasComponent implements OnInit {
  unidades: Unidades
  resposta_unidades :any = []

  public vencimento_gratis : string

  public propostaScreen : boolean = true
  public responsePropostaScreen : boolean = false
  public responseScreen : boolean = false
  public errors_input : boolean = false
  public estadoLoad : boolean = false
  public estadoLoadUnidades : boolean = false
  public inputsCadastroUnidade : boolean = false
  public keepCadastro : boolean = false

  public response_proposta : any

  public estadoForm : string = "disabled"
  public estadoForm2 : string = "disabled"

  public etapa : number = 1

  public mensagem_loader : string

  public codigoValido : boolean = false
  public nome_unidadeValido : boolean = false
  public cepValido : boolean = false
  public bairroValido : boolean = false
  public valor_trocoValido : boolean = false
  public cnpjValido : boolean = false
  public ruaValido : boolean = false
  public cidadeValido : boolean = false
  public numeroValido : boolean = false
  public estadoValido : boolean = false
  public img_matriz : string = "/assets/new_user_img.svg"
  public img_base64 : string = ""

  public codigo_proposta : string

  public valor_mensalidade : number

  public propostaForm : FormGroup = new FormGroup({
    'codigo' : new FormControl(null)
  })

  public p
  dtElement: DataTableDirective;
  ativo: boolean = false
  alldados: any = []
  dtOptions: DataTables.Settings = {}
  dtTrigger: Subject<any> = new Subject()
  data: any
  public cep = [/\d/,/\d/,/\d/,/\d/,/\d/, '-', /\d/, /\d/, /\d/]
  public cnpj = [/\d/,/\d/,'.',/\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/, '/', /\d/, /\d/, /\d/, /\d/,'-',/\d/,/\d/,]
  public formLojas :FormGroup
  constructor(private formBuilder: FormBuilder, 
    private http: HttpClient, 
    private serv: InserviceService,
    private errorMessageService : ErrorInputService,
    private responsesModuleService : ResponsesModuleService,
    private requests : Requests,
    private validaCnpj : ValidaCnpj,
    private editUnidadeService : EditUnidadeService,
    private rotas : Router) { }

  ngOnInit() {

    ImagemModuleService.emitirImagemBase64.subscribe(
      imagem => {
        this.img_matriz = imagem
        this.img_base64 = imagem
      }
    )

    this.requests.getUnidades()
    .subscribe(
      data => {
        this.fecharLoad(false)
        this.resposta_unidades = data
        if(this.resposta_unidades.code === 200){
          this.unidades = this.resposta_unidades.data
        }else{
          
        } 
    })

    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        if(trigger){
          $('#exampleModall').modal('hide');
        }else{
          this.mostrarResponseScreen(false, null, null, null)
          this.inputsCadastroUnidade = (this.keepCadastro) ? true : false
        }
      }
    )

    this.listaUnidade()
    this.formLojas = this.formBuilder.group({
      'id' : ['', []],
      'nome_unidade' : ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      'cep' : ['', [Validators.required, Validators.minLength(8), Validators.maxLength(10)]],
      'bairro' : ['', Validators.required],
      'valor_troco' : ['', [Validators.required]],
      'cnpj' : ['', [Validators.required, Validators.minLength(14), Validators.maxLength(14)]],
      'rua' : ['',[ Validators.required]],
      'cidade' : ['', [Validators.required]],
      'numero' : ['', [Validators.required]],
      'estado' : ['', [Validators.required]]
    })

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  consultaCep(){
    let cep = this.formLojas.get('cep').value
    let novocep = cep.replace('-','')
    if(cep != ''){
      var validacep = /^[0-9]{8}$/;
      if(validacep.test(novocep)){

        this.serv.buscaCep(novocep).subscribe(dados => {
          
          //console.log(dados)
          this.formLojas.get('bairro').setValue(dados.bairro)
          this.formLojas.get('rua').setValue(dados.logradouro)
          this.formLojas.get('estado').setValue(dados.uf)
          this.formLojas.get('cidade').setValue(dados.localidade)
          this.validaBairro(dados.bairro)
          this.validaRua(dados.rua)
          this.validaCidade(dados.cidade)
          this.validaEstado(dados.estado)
        })

      }
    }
  }

  listaUnidade(){
    this.mostrarLoaderUnidades(true, "Carregando Unidades")
    this.serv.buscaListaUnidade().subscribe(dados => {
      this.mostrarLoaderUnidades(false, null)
      this.data = dados.data
      
      this.alldados = dados
    
    })
  }

  editar(dados){
    this.editUnidadeService.enviaInfoUnidadesConfig(dados)
    this.rotas.navigate(['dashboard/atualizar-unidade'])
  }

  cadastraUnidade(){
    this.inputsCadastroUnidade = false
    this.mostrarLoader(true, "Cadastrando Unidade")
    this.serv.gravaUnidade(
      this.codigo_proposta,
      this.formLojas.value.nome_unidade, 
      this.formLojas.value.cep, 
      this.formLojas.value.bairro, 
      this.formLojas.value.valor_troco, 
      this.formLojas.value.cnpj, 
      this.formLojas.value.rua, 
      this.formLojas.value.cidade,
      this.formLojas.value.numero,
      this.formLojas.value.estado,
      this.img_base64)
      .subscribe(
        dados => {
          this.mostrarLoader(false, null)

          if(dados.code == 200){
            this.keepCadastro = false
            this.formLojas.reset()
          }else{
            this.keepCadastro = true
            this.formLojas.reset();
          }
          this.mostrarResponseScreen(true, 
            dados.code,
            dados.mensagem,
            "300px")
        }
    )
  }

  verificaProposta(){

    if(this.propostaForm.value.codigo != null){
      if(this.propostaForm.value.codigo.length>0){
      this.mostrarErrordeInput(false)
      this.mostrarLoader(true, "Buscando Proposta")

      let proposta : any = {
        codigo_proposta: this.propostaForm.value.codigo
      }

      
      this.requests.buscarProposta(proposta)
      .subscribe(
        res => {
          this.mostrarLoader(false, null)

          var resp_proposta : any = res
          if(resp_proposta.code===200){
            this.response_proposta = resp_proposta.data[0]
            this.vencimento_gratis = this.nextMonth()
            this.codigo_proposta = this.propostaForm.value.codigo
            this.valor_mensalidade = this.response_proposta.valor_mensalidade.replace(".",",")
            this.mostrarResponseProposta(true)
          }else{
            this.mostrarResponseScreen(
              true,
              resp_proposta.code,
              resp_proposta.mensagem,
              "300px"
            )
          }
        }
      )
      }else{
        this.mostrarErrordeInput(true)
        this.errorMessageService.enviaMensagemErro("Você deve inserir um código de proposta.")
      }
    }else{
      this.mostrarErrordeInput(true)
      this.errorMessageService.enviaMensagemErro("Você deve inserir um código de proposta.")
    }
  }

  prosseguirCadastro(){
    this.inputsCadastroUnidade = true
    this.mostrarResponseProposta(false)
  }

  getImgMatriz(){
    this.requests.solicitarImgMatriz()
    .subscribe(
      res => {
        var resp_img : any = res
        if(resp_img.code === 200){
          this.img_matriz = this.requests.apiUrlImg+resp_img.data
        }
        this.img_base64 = ""
      }
    )
  }

  mostrarErrordeInput(status :boolean){
    this.errors_input = status
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarResponseProposta(status: boolean){
    this.responsePropostaScreen = status
  }

  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarLoaderUnidades(status: boolean, mensagem: string){
    this.estadoLoadUnidades = status
    this.mensagem_loader = mensagem
  }

  fecharLoad(status :boolean) :void{
    this.estadoLoad = status;
  }

  validaCodigo(codigo : string){
    if(this.propostaForm.value.codigo.length>5){
      this.codigoValido = true
      
    }else{
      this.codigoValido = false
    }
    this.ativarBotaoProposta()
  }

  validaNome(nome : string){
    if(this.formLojas.value.nome_unidade.length>5){
      this.nome_unidadeValido = true
      
    }else{
      this.nome_unidadeValido = false
    }
    this.ativarBotao()
  }

  validaBairro(bairro : string){
    if(this.formLojas.value.bairro.length>4){
      this.bairroValido = true
      
    }else{
      this.bairroValido = false
    }
    this.ativarBotao()
  }

  validaRua(rua : string){
    if(this.formLojas.value.rua.length>1){
      this.ruaValido = true
      
    }else{
      this.ruaValido = false
    }
    this.ativarBotao()
  }

  validaCidade(cidade : string){
    if(this.formLojas.value.cidade.length>2){
      this.cidadeValido = true
      
    }else{
      this.cidadeValido = false
    }
    this.ativarBotao()
  }

  validaCep(cep){
    cep = this.formLojas.value.cep.replace("-","")
    var cepFinal = cep.replace(/_/g,"")
      if(cepFinal.toString().length == 8){
        this.cepValido = true
      }else{
        this.cepValido = false
      }
      this.ativarBotao()
  }


  validaEstado(estado : string){
    if(this.formLojas.value.estado.length == 2){
      this.estadoValido = true
      
    }else{
      this.estadoValido = false
    }
    this.ativarBotao()
  }

  validaNumero(numero){
    if(this.formLojas.value.numero>0){
      this.numeroValido = true
    }else{
      this.numeroValido = false
    }
    this.ativarBotao()
  }

  validaCNPJ(cnpj){
    this.cnpjValido = this.validaCnpj.validarCNPJ(cnpj.value)
    this.ativarBotao()
  }

  ativarBotaoProposta(){
    if(this.codigoValido != false){
      this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }
  }

  ativarBotao(){
    if(this.nome_unidadeValido == true && this.cnpjValido == true && this.cepValido == true && this.ruaValido== true 
      && this.bairroValido == true && this.estadoValido == true && this.numeroValido == true){
      this.estadoForm2 = ''
    }else{
      this.estadoForm2 = 'disabled'
    }
  }

  nextMonth(){
    var today = new Date
    var dia = today.getDate()
    var mes = today.getMonth()+1
    var ano = today.getFullYear()

    if(mes>12){
      mes = 12
    }

    if(dia>9){
      var vencimento_final = dia+"/"+mes+"/"+ano
    }else{
      var vencimento_final = "0"+dia+"/"+mes+"/"+ano
    }

    return vencimento_final
  }

}
