import { Component, OnInit } from '@angular/core';
import { Unidades } from '../../../../models/unidade.model';
import { UnidadeSelected } from '../../../../models/unidade_selected.model';
import { Requests } from '../../../../services/requests.service';
import { UnidadeSelectedService } from '../../../../services/unidade_selected.service';

@Component({
  selector: 'app-unidades',
  templateUrl: './unidades.component.html',
  styleUrls: ['./unidades.component.css'],
  providers: [UnidadeSelectedService]
})
export class UnidadesComponent implements OnInit {

  public data : any
  unidades: Unidades
  unidade_selected: UnidadeSelected
  resposta_unidades :any = []

  estadoLoad: boolean = true

  unidade_selecionada :number = 0
  status_unidade_select :boolean = false;

  unidade_selecionada_nome : string = "Nenhuma Unidade Selecionada"

  titulo_pagina : string

  constructor(private request :Requests, private unidadeSelectedService :UnidadeSelectedService) { }

  ngOnInit() {
    this.getTituloPagina()

    this.request.getUnidades()
    .subscribe(
      data => {
        this.fecharLoad(false)
        this.resposta_unidades = data
        if(this.resposta_unidades.code === 200){
          this.unidades = this.resposta_unidades.data
        }else{
          
        } 
    })

    UnidadeSelectedService.emitirRefreshUnidades.subscribe(
      trigger => {
        if(trigger === true){
          this.fecharLoad(true)
          this.buscarUnidades()
        }
      }
    )

  }

  buscarUnidades(){
    this.request.getUnidades()
          .subscribe(
            data => {
              this.fecharLoad(false)
              this.resposta_unidades = data
              if(this.resposta_unidades.code === 200){
                this.unidades = this.resposta_unidades.data
              }else{
                
              } 
          })
  }

  mudarParametro(id, nome_fantasia, endereco, numero, bairro, cidade, saldo){
    this.unidade_selected = { 
      id, nome_fantasia, endereco, numero, bairro, cidade, saldo 
    }

    this.unidadeSelectedService.enviaUnidadeSelecionada(this.unidade_selected)
    this.mostrarUnidadeSelecionada(true)
  }

  fecharLoad(status :boolean) :void{
    this.estadoLoad = status;
  }

  mostrarUnidadeSelecionada(status : boolean) :void{
    this.status_unidade_select = status
  }

  getTituloPagina(){
    this.titulo_pagina = this.unidadeSelectedService.getTituloPagina()
  }

}
