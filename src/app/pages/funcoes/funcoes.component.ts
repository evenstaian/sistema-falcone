import { InserviceService } from 'src/services/inservice.service';
import { DataTableDirective } from 'angular-datatables';
import { Unidades } from './../../../models/unidade.model';
import { element } from 'protractor';
import { TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { ValidaCpf } from './../../../utils/valida-cpf';
import { FormGroup, FormControl } from '@angular/forms';
import { Requests } from 'src/services/requests.service';
import { AddColaborador } from 'src/models/add_colaborador.model';
import { ErrorInputService } from 'src/services/modules_services/error_input.service';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { ColaboradoresSelectedService } from 'src/services/modules_services/colaboradores.service';
import { UnidadeSelectedService } from 'src/services/unidade_selected.service';

@Component({
  selector: 'app-funcoes',
  templateUrl: './funcoes.component.html',
  styleUrls: ['./funcoes.component.css'],
  providers: [
    ErrorInputService, 
    ResponsesModuleService, 
    ColaboradoresSelectedService,
    UnidadeSelectedService,
    ValidaCpf
  ]
})
export class FuncoesComponent implements OnInit {
  public estadoForm : string = "disabled"

  public estiloBotaoAtivo : any

  public nomeValido : boolean = false
  public cpfValido : boolean = false
  public emailValido : boolean = false
  public confEmailValido : boolean = false
  public perfilColaboradorValido : boolean = false

  public formFuncoes :FormGroup = new FormGroup({
    'nome_colaborador' : new FormControl(null),
    'email'  : new FormControl(null),
    'cpf'  : new FormControl(null),
    'confirmar_email' : new FormControl(null),
    'perfil_colaborador' : new FormControl(null)
  })

  public perfil_colaborador : string
  public id_unidade : number
  public nome_unidade : string = "Unidade de Loja"

  public email_novo_colaborador : string
  public mensagem_confirmacao : string

  public inputsAddColaborador : boolean = true
  public errors_input : boolean = false
  public responseScreen : boolean = false
  public estadoLoad : boolean = false
  public mensagem_loader : string = "Carregando Dados"

  public colaborador : any
  public funcionalidade : any
  public cargo : string
  public is_admin : boolean

  public cpf_mask = [/\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '.', /\d/, /\d/,/\d/, '-', /\d/, /\d/]


  constructor(private requests: Requests, 
    private errorInputService : ErrorInputService,
    private responsesModuleService : ResponsesModuleService,
    private colaboradoresSelectedService : ColaboradoresSelectedService,
    private validaCpf : ValidaCpf,
    private serv: InserviceService,
    private unidadeSelectedService :UnidadeSelectedService) { }

  ngOnInit() {
    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      res => {this.mostrarResponseScreen(res, !res, null, null, "727px")}
    )

    UnidadeSelectedService.emitirUnidadeSelecionada.subscribe(
      unidade_selected => {
        this.id_unidade = unidade_selected.id
        this.nome_unidade = unidade_selected.nome_fantasia
      }
    )

    this.ativarBotao()

    var dados_conta = JSON.parse(localStorage.getItem('dados_conta'))
    this.colaborador = dados_conta.colaborador
    this.funcionalidade = dados_conta.funcionalidades

    this.is_admin = this.colaborador[0].admin===1?true:false
    this.cargo = this.colaborador[0].cargo

  }

  cadastrarColaborador(){
    if(this.formFuncoes.value.perfil_colaborador != null){
      this.perfil_colaborador = this.formFuncoes.value.perfil_colaborador

      if(this.perfil_colaborador === "AC" ||
        this.perfil_colaborador === "GC" ||
        this.perfil_colaborador === "GU"  
      ){

        if(this.id_unidade != null){

          if(this.formFuncoes.value.email === this.formFuncoes.value.confirmar_email){
            this.mostrarLoader(true, false, "Cadastrando Novo Colaborador")

            let add_colaborador = new AddColaborador(
              this.perfil_colaborador,
              this.id_unidade,
              this.formFuncoes.value.nome_colaborador,
              this.formFuncoes.value.cpf,
              this.formFuncoes.value.email
            )
        
            this.requests.cadastrarColaborador(add_colaborador)
              .subscribe(
                res => {
                  this.mostrarLoader(false, true, null)
                  var resposta_cadastro : any = res
                  if(resposta_cadastro.code === 200){

                    this.colaboradoresSelectedService.refreshColaboradores(true)

                    this.mostrarResponseScreen(
                      true, 
                      false, 
                      resposta_cadastro.code, 
                      resposta_cadastro.mensagem, 
                      "500px")
                      this.formFuncoes.reset()
                  }else{
                    this.mostrarResponseScreen(
                      true, 
                      false, 
                      resposta_cadastro.code, 
                      resposta_cadastro.mensagem, 
                      "500px")
                  }
                }
              )

              this.mostrarErrosInput(
                false, 
                null
                )
          }else{
            this.mostrarErrosInput(
              true,
              "Há conflitos entre o email e a confirmaçao dele."
              )
          }
        }else{
          this.mostrarErrosInput(
            true,
            "Você deve escolher uma unidade onde irá adicionar o colaborador."
            )
        }
      }
    }else{
      this.mostrarErrosInput(
        true, 
        "Você deve escolher um perfil de permissão para o Colaborador!"
        )
    }
  }

  mostrarErrosInput(status:boolean, mensagem: string){
    this.errors_input = status;
    this.errorInputService.enviaMensagemErro(mensagem);
  }

  mostrarResponseScreen(status: boolean, inverso: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.inputsAddColaborador = inverso
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }

  mostrarLoader(status: boolean, inverso: boolean, mensagem: string){
    this.estadoLoad = status
    this.inputsAddColaborador = inverso
    this.mensagem_loader = mensagem
  }

  mensagemConfirmacao(){
    this.email_novo_colaborador = this.formFuncoes.value.email

    if(this.email_novo_colaborador != undefined){
      this.mensagem_confirmacao = `Um email de ativação do cadastro, 
      contendo as informações de acesso será enviado para `
    }else{
      this.mensagem_confirmacao = "Insira todos os dados requeridos!"
    }
    
  }

  validaNome(nome : string){
    if(this.formFuncoes.value.nome_colaborador.length<7 || this.formFuncoes.value.nome_colaborador.indexOf(" ") == -1){
      this.nomeValido = false
      
    }else{
      this.nomeValido = true
    }
    this.ativarBotao()
  }

  validaCPF(cpf){
    this.cpfValido = this.validaCpf.validarCPF(cpf.value)
    this.ativarBotao()
  }

  validaEmail(email_teste: string){
    if(this.formFuncoes.value.email.indexOf('@') == -1 || this.formFuncoes.value.email.indexOf('.') == -1 ){
      this.emailValido = false
    }else{
      this.emailValido = true
    }
    this.ativarBotao()
  }

  validaConfEmail(email_teste2: string){
    if(this.formFuncoes.value.confirmar_email.indexOf('@') == -1 || this.formFuncoes.value.confirmar_email.indexOf('.') == -1 ){
      this.confEmailValido = false
    }else{
      this.confEmailValido = true
    }
    this.ativarBotao()
  }

  ativarBotao(){
    if(this.nomeValido == true && this.cpfValido == true && this.emailValido && this.confEmailValido == true){
        this.estadoForm = ''
    }else{
      this.estadoForm = 'disabled'
    }

    this.estiloBotaoAtivo = {
      'opacity': (this.estadoForm === 'disabled' ? '0.8' : '1'),
      'cursor' : (this.estadoForm === 'disabled' ? 'none' : 'pointer'),
      'pointer-events' : (this.estadoForm === 'disabled' ? 'none' : 'auto')
    }
  }
}
