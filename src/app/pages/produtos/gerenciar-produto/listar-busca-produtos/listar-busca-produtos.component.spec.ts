import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarBuscaProdutosComponent } from './listar-busca-produtos.component';

describe('ListarBuscaProdutosComponent', () => {
  let component: ListarBuscaProdutosComponent;
  let fixture: ComponentFixture<ListarBuscaProdutosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarBuscaProdutosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarBuscaProdutosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
