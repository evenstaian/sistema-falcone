import { OperadorSelectedService } from './../../../../services/modules_services/operador_selected.service';
import { DataTableDirective } from 'angular-datatables';
import { UnidadeSelectedService } from 'src/services/unidade_selected.service';
import { Component, OnInit } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { InserviceService } from '../../../../services/inservice.service';
import { FormsModule, FormGroup, FormControl } from '@angular/forms';
import { AcessPagesService } from 'src/services/modules_services/acess_pages.service';
import { Subject } from 'rxjs';
declare var $;
declare let jsPDF;

@Component({
  selector: 'app-trocos-passados',
  templateUrl: './trocos-passados.component.html',
  styleUrls: ['./trocos-passados.component.css'],
  providers: [ 
    UnidadeSelectedService,
    AcessPagesService,
    OperadorSelectedService ]
})
export class TrocosPassadosComponent implements OnInit {
  estadoLoad : boolean = false

  public estadoAcess : boolean = false
  status_unidade_select :boolean = false
  status_extrato :boolean = true
  unidade_selecionada_nome : string = "Nenhuma Unidade Selecionada"
  msg_no_extrato : string

  estadoNum : boolean = false

  unidades: any
  operadores: any
  unidadeEscolhida: string = "Unidade de loja"
  idunidadeEscolhida: number
  id_unidade: number
  idoperadorEscolhido: number
  operadorEscolhido: string = "Operador de caixa"
  verifica: any
  num_troco: any
  valor_troco: any
  lista_trocos: any

  public showCelular : string = "0"
  
  public p

  inicio
  fim

  dtElement: DataTableDirective;
  ativo: boolean = false
  alldados: any = []
  dtOptions: DataTables.Settings = {}
  dtTrigger: Subject<any> = new Subject()

  constructor(private serv: InserviceService, private form: FormsModule,
    private acessPagesService : AcessPagesService,
    private operadorSelectedService : OperadorSelectedService) { }

  public formTroco : FormGroup = new FormGroup({
    'model1' : new FormControl(null),
    'model2' : new FormControl(null),
  })

  public model1: any = this.formTroco.value.model1;

  public model2: any = this.formTroco.value.model2;

  public myDatePickerOptions1: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
  }

  public myDatePickerOptions2: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
  }

  ngOnInit() {
    this.operadorSelectedService.enviaUnidadeSelecionada(0)

    OperadorSelectedService.emitirOperadorSelecionado.subscribe(
      operador => {
        this.idoperadorEscolhido = operador.id
        this.operadorEscolhido = operador.nome

      }
    )

    AcessPagesService.emitirAcessPagesConfig.subscribe(
      data => this.estadoAcess = true
    )

    UnidadeSelectedService.emitirUnidadeSelecionada.subscribe(
      unidade_selected => {
        this.idunidadeEscolhida = unidade_selected.id
        this.unidadeEscolhida = unidade_selected.nome_fantasia
      }
    )

    this.acessPagesService.enviaAcessPagesConfig(
      [{
        page: "Extrato",
        style: "link_blur",
        router: "extrato"
      },
      {
        page: "Extrato de trocos passados",
        style: "link_ativo",
        router: "trocos-passados"
      }]
    )

    //opções do datatable
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    }

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  //função para renderizar o novo datatble
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {     
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  onSubmit(form){
    this.fecharLoad(true)
    
    let d1
    let d2

    let id_loja = this.idunidadeEscolhida
    let id_operador = this.idoperadorEscolhido
    if(this.formTroco.value.model1.formatted !== undefined){
      d1 = this.formTroco.value.model1.formatted;
      this.inicio = d1
     }else{
      d1 = '';
     }
     if(this.formTroco.value.model2.formatted !== undefined){
      d2 = this.formTroco.value.model2.formatted;
      this.fim = d2
     }else{
      d2 = '';
     }
     let separa1 = d1.split("/")
     let separa2 = d2.split("/")
    // console.log(d2)
     let ndata1 = separa1[2]+'-'+separa1[1]+'-'+separa1[0] + ' 00:00:00'
     let ndata2 = separa2[2]+'-'+separa2[1]+'-'+separa2[0] + ' 23:59:59'
     //let all = id_loja + ' - ' + id_operador +' - '+ ndata1 +' - '+ ndata2
     //console.log(all)

     this.serv.enviaDadosExtrato(id_loja, id_operador, ndata1, ndata2).subscribe(dados => {
     
      if(dados.code===200){
        this.mostrarUnidadeSelecionada(true)
        if(dados.data.dados_totais[0].num_trocos>0){
          this.fecharLoad(false)
          this.num_troco = dados.data.dados_totais[0].num_trocos
          this.valor_troco = dados.data.dados_totais[0].valor_trocos
          this.lista_trocos = dados.data.lista_trocos
          this.mostrarExtrato(true)
        }else{
          this.fecharLoad(false)
          this.mostrarExtrato(false)
          
        }
      }else{
        this.fecharLoad(false)
        this.mostrarExtrato(false)
        
      }
     })
  }

  escolheUnidade(){
    this.operadorSelectedService.enviaUnidadeSelecionada(this.idunidadeEscolhida)
  }

  escolheOperador(operadores){
    this.verifica = operadores
    if(this.verifica == 'todos'){
      this.idoperadorEscolhido = 0
      this.operadorEscolhido = 'Todos'
    }
  }

  convertToPDF(){

    //if(this.formExport.value.tipo_export!=null){
      //(this.formExport.value.tipo_export==="pdf"){
        var item = this.lista_trocos
      
        var pdfsize = 'a4';
        var doc = new jsPDF('landscape','pt', pdfsize);

        var titulo = 'Trocos Passados ('+ this.inicio + ' a ' + this.fim + ')'
        var subtitle = "Quantidade de Trocos: " + this.num_troco + " | " 
        + "Valor Total: " + this.formataValor(this.valor_troco)

        var img = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAF70lEQVRo3tVaXWwUVRT+zmxZS60rwYZARWKqEgJGSSAkYiRASZWftpZEYrRoJDwYQowhPvighBAejDEEkYfGHwxEE1KbSBraCBIi5cewxQrYlFpb6PZn2y6lXXa3092Z2Xt8mG73d3Zmt7tWTnIy39575+z57j3n7J07S8xMeIhFmm0HZioF+TReNfyuxMzFAOYT0TwwikEoZuZCAIVEVMjMBQAVEEECQ2IARAyAwACIIUCsAaQB0Jg5CEAm4E7jopMdlKsQqnLXFhDRGgCbGLQGwFIApQCK8jQ/YqLfv2zGK7DNXfs8EXYD2CGYF+qtDAL0GUSkJb2kGm/UNoWlnroOKWsCle6dqwEcJNBrEe8Sl5IMcDoSidjIhjoe8ri+7+rNmMDWwZ0OAr4g0C4AEoiBfBayyDIkYM2vtjGzkhGBbQO1K4nQAKCMI5YYMA+QGQinxqpfcQIZVKHK/tp1RGgEw5E/b62L7PI7AYu/A1tdb78ggEbBcAgGjJXygJM1rLLWXdfRZmkFNne9WWwrLGgA4EgMRyCxMnBMmCbjyJioEZPxU+lFkWidwsGxYO+DP+57LBEgu+0TBp4Fc4Ib2eNpyqY2dQbRfNOxOqFeZ2ZhSqDi1hsldod9L5hjvhz/MU5uU8f1BDYlIBXa3hHgouyKjEH9yxgn2/XeGnWiespHExeqiAFiQvRqhnOtFIfDE2rg789vtk9PspHzCyqetINoNTPAzIhezTAQf09useINdWp+1WcaQkveeq6UJMoyfPIn4QnNGfvZkIDdYS+JJu//R0L3JuMIGIaQCAt7fEhkqrkIm4QQFYzhXwesEVDHQko0trNRqzmTDscTU7yKp+/EP3csEbh3eWiUmGMqgFXMMW25xZpfucHMiiUC/T/2eFS/5hPM0BWwhhHTllusBlRnop+GBFiwrIwH2/NT27PTyYGAdQIAEBye/GVmeZA7FarQek90tWVEwPVDV0M4GFaQfg+dZwUgGMpYsNfbOjqcEQHPucEuX8d488zK6UxVr0KarLVFdqCWCTCzcJ3oOqQFVHm2q5D6QHGm8tH0iWzk14E2d1Pf4dmuQr7b4ykJWDrYIqKilUfX1pe8smiL2dh8SDioyZdebS5VHyi+xD5Lz8TMLN/c9/uu0avDLXpgIiZI84n1q+pVOlM5b5kAAAhVeG7svbLD3dR3WoSFyPe2OT6BVaeRX5YJlF+rKSpvrZnf8en12u4v/zoQ8kz69BkyUs4RBpT7oZkTIAl1YLSVt9bsf+b95V+37vqtYqjJ1awFlGDq+percGLcaxkyJGApicudNXtBdDTmOVUBcC48GW4ZOuMqKXqqeMu8lU8slQptdqsTYlVUnzp6aVPTkqlj9cwJlF97fS0RXQCQc+esyOSQfP5K9dkKo/60pxIbrlYvJBvVM7M9d6cMmWCCOmGcwGkJrL9YWWB7xHYKjFLdJMf0csynhFM3C+1RF9OPJ2IE3XJaAoZJTHbpMwGsE9D3U+EEFTGaqi3SLg/LXmHQJ0zsaBpr/T/1tCGNpCSw8Ur1Dgm0T2JAYv3FVfSaDiOu3XN+8AwxLqTuN1fNG+rzOpN3oGlDaMPl6uWQ8C1Pr242Ly8YI2f76+W+iY8XbCxtF5yFCUzvQDXLK7D+YqUDhAYCimnqV4SmFRYwA0IId6Pr5O2Df+5++r2l2wkoojg71lXzKWnjP24FXm7eLNkdc44TsEw/D6KpuWeYvm6LjAyzGPj57vHuI+0fvvjVS0Ei2hOxlXkVAvzdPlMC0ysw57E5HwG03fyIIzUOa0L01fcc6z7S/gEzy4+vmL8FRGXZ7oW0oCbfqeu4ZWkFNrRUbSTCIeMT4fTCitD6T/UcvvtN5/7IsQdJtCdbewCg3g91al7VZzauYMXBVfMeLXMck2xkOjjJcYYIDsuekXODR0fODhyPJBwRSau+W1dmm2sbS3UfJdsB9MoJANACqjx4uvdAqkfIVLbmAlic5UQJAB5mDiQZJloMoDBDWxEJMLPHyk05+6vBbMlD/2+VfwGFNKxSQ0qKmwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOC0wMS0wOFQwMjoxMDo0Ny0wNTowMGZzLwgAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTgtMDEtMDhUMDI6MTA6NDctMDU6MDAXLpe0AAAAAElFTkSuQmCC'

        var header = function(data) {
          doc.setFontSize(12);
          doc.setTextColor(40);
          doc.setFontStyle('normal');
          doc.addImage(img, 'PNG', data.settings.margin.left, 20, 25, 25);
          doc.text(titulo,
          40, 30);
          doc.text(subtitle,
            40, 45);
        }

        doc.setFontSize(10)
        var col = ["Operador", "Unidade", "Código", "Data e Hora", "Celular", "Valor"];
        var rows = [];
        
        for(var i in item){
            var temp = [
              this.lista_trocos[i].operador,
              this.lista_trocos[i].unidade,
              this.lista_trocos[i].codigo,
              this.formataDiaHora(this.lista_trocos[i].data),
              this.formataCelular(this.lista_trocos[i].celular),
              this.formataValor(this.lista_trocos[i].valor)];
            rows.push(temp);
        }
      
    
        doc.autoTable(col, rows, {
          drawHeaderRow: function(row, data) {
            row.height = 20;
            row.styles.fillColor = [139, 189, 90];
          },
          drawRow: function(row, data) {
            row.height = 20;
          },
          beforePageContent: header,
          //addPageContent: header2,
          startY: 60,
          margin: {horizontal:10,top: 10},
          //startY: 0.10*doc.internal.pageSize.height,
          styles: {
            fontSize: 10,
            overflow: 'linebreak',
            valign: 'middle'
          },
          columnStyles: {
            operador: {columnWidth: 30}, 
            unidade:{columnWidth:40}, 
            data:{columnWidth: 15}, 
            cod_fechamento: {columnWidth: 15}, 
            valor_total: {columnWidth: 15}
          }
        });
    
        doc.save('Trocos Passados ('+ this.inicio + ' a ' + this.fim + ').pdf');
      //}
    //}
  }

  formataDiaHora(datetime : string){
    if(datetime!=null){
      var d = datetime.substring(8,10)
      var m = datetime.substring(5,7)
      var y = datetime.substring(0,4)
      var h = datetime.substring(11,19)
      var result = d + "/" + m + "/" + y + " " + h
      return result
    }else{
      return ''
    }
  }

  formataCelular(celular : string){
    if(celular!=null){
      var result = "(" 
      + celular.substring(0,2) 
      + ")*****-" 
      + celular.substring(7,12)
      return result
    }else{
      return ''
    }
  }

  formataValor(valor : string){
    if(valor!=null){
      var sufixo = "R$ "
      var result = sufixo + valor.replace(".",",")
      return result
    }else{
      return ''
    }
  }

  mostrarCelular(celular){
    this.showCelular = celular
  }

  mostrarUnidadeSelecionada(status : boolean){
    this.status_unidade_select = status
  }

  mostrarExtrato(status :boolean){
    this.msg_no_extrato = "Não há transações nesse periodo para a unidade e operador selecionados."
    this.status_extrato = status
  }

  fecharLoad(status : boolean){
    this.estadoLoad = status
  }

}
