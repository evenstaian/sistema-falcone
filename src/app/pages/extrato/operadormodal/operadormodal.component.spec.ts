import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperadorModalComponent } from './operadormodal.component';

describe('OperadormodalComponent', () => {
  let component: OperadorModalComponent;
  let fixture: ComponentFixture<OperadorModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperadorModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperadorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
