import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservasInseridasComponent } from './reservas-inseridas.component';

describe('ReservasInseridasComponent', () => {
  let component: ReservasInseridasComponent;
  let fixture: ComponentFixture<ReservasInseridasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservasInseridasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservasInseridasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
