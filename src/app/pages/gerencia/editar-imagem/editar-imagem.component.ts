import { Component, OnInit} from '@angular/core';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { ResponsesModuleService } from 'src/services/modules_services/responses_module.service';
import { Requests } from 'src/services/requests.service';

@Component({
  selector: 'app-editar-imagem',
  templateUrl: './editar-imagem.component.html',
  styleUrls: ['./editar-imagem.component.css'],
  providers: [
    ImageCropperComponent,
    ResponsesModuleService]
})
export class EditarImagemComponent implements OnInit {

  corteScreen : string = "p1"

  data: any;
  cropperSettings: CropperSettings;

  estadoLoad : boolean = false;
  mensagem_loader : string = "Carregando Dados"

  responseScreen : boolean = false

  constructor(
    public responsesModuleService : ResponsesModuleService,
    private requests : Requests
    ) {
      this.cropperSettings = new CropperSettings();
      this.cropperSettings.width = 500;
      this.cropperSettings.height = 500;
      this.cropperSettings.croppedWidth = 500;
      this.cropperSettings.croppedHeight = 500;
      this.cropperSettings.canvasWidth = 400;
      this.cropperSettings.canvasHeight = 300;

      this.data = {};
    }

  ngOnInit() {
    ResponsesModuleService.emitirFecharResponseModule.subscribe(
      trigger => {
        this.corteScreen = "p1"
        this.mostrarResponseScreen(false, null, null, null)
        this.data = {}
      }
    )
  }

  finishEdition(){
    if(this.data.image){
      this.corteScreen = "p2"
    }
  }

  confirmarFoto(){
    this.corteScreen = "p3"
    this.mostrarLoader(true, "Enviando Imagem")
    var dados_imagem : any = {
      img_negocio : this.data.image
    }
    this.requests.mudarImagemNegocio(dados_imagem).subscribe(
      res => {
        this.mostrarLoader(false, null)
        var resp_imagem : any = res
        if(resp_imagem.code === 200){

        }
        this.mostrarResponseScreen(
          true, 
          resp_imagem.code,
          resp_imagem.mensagem,
          "100%")
      }
    )
  }
  
  mostrarLoader(status: boolean, mensagem: string){
    this.estadoLoad = status
    this.mensagem_loader = mensagem
  }

  mostrarResponseScreen(status: boolean, code: number, mensagem: string, altura: string){
    this.responseScreen = status
    this.responsesModuleService.enviaConfigResponse(code, mensagem, altura)
  }
}
