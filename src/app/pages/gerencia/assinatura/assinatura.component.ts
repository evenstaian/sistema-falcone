import { Component, OnInit } from '@angular/core';
import { AcessPagesService } from 'src/services/modules_services/acess_pages.service';


@Component({
  selector: 'app-assinatura',
  templateUrl: './assinatura.component.html',
  styleUrls: ['./assinatura.component.css'],
  providers: [ AcessPagesService ]
})
export class AssinaturaComponent implements OnInit {

  public estadoAcess : boolean = false

  constructor(private acessPagesService : AcessPagesService) { }

  ngOnInit() {
    AcessPagesService.emitirAcessPagesConfig.subscribe(
      data => this.estadoAcess = true
    )

    this.acessPagesService.enviaAcessPagesConfig(
      [{
        page: "Gerenciar conta",
        style: "link_blur",
        router: "gerencia"
      },
      {
        page: "Assinatura e cobrança",
        style: "link_ativo",
        router: "assinatura"
      }]
    )
  }

}
