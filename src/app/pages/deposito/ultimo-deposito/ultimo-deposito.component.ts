import { Component, OnInit } from '@angular/core';
import { Requests } from 'src/services/requests.service';
import { LastDeposito } from 'src/models/last_deposito.model';
import { UltimoDepositoModuleService } from 'src/services/modules_services/ultimo_deposito_module.service';
import { UnidadeSelectedService } from 'src/services/unidade_selected.service';

@Component({
  selector: 'app-ultimo-deposito',
  templateUrl: './ultimo-deposito.component.html',
  styleUrls: ['./ultimo-deposito.component.css'],
  providers: [ UnidadeSelectedService, UltimoDepositoModuleService ]
})
export class UltimoDepositoComponent implements OnInit {

  id_unidade : string
  
  public mensagem_not_selected : string = "Nenhuma Unidade Selecionada"

  public status_unidade_select : boolean = false
  public status_last_deposito : boolean = true

  public estadoLoad : boolean = false

  last_deposito : LastDeposito = {
    codigo: "",
    unidade: 0,
    valor: 0,
    data: "",
    status: ""
  }

  constructor(private requests : Requests) { }

  ngOnInit() {

    UnidadeSelectedService.emitirUnidadeSelecionada.subscribe(
      UnidadeSelected => {
        var unidadeSelecionada : any = UnidadeSelected
        this.id_unidade = unidadeSelecionada.id
        this.solicitarUltimoDeposito(this.id_unidade)
      }
    )

    UltimoDepositoModuleService.emitirRefreshUltimoDeposito.subscribe(
      trigger => {
        if(trigger === true){
          this.solicitarUltimoDeposito(this.id_unidade)
        }
      }
    )

  }

  verFatura(url_fatura){
    window.open(url_fatura, '_blank');
  }

  solicitarUltimoDeposito(id_unidade : string){
    this.status_unidade_select = true
    this.status_last_deposito = true
    this.fecharLoad(true)

    this.requests.solicitarUltimoDeposito(id_unidade)
    .subscribe(
      res => {
        this.fecharLoad(false)
        var resposta_last_deposito : any = res
        if(resposta_last_deposito != null){
          if(resposta_last_deposito.code === 200){
            this.last_deposito = resposta_last_deposito.data
            if(this.last_deposito!=null){
              this.status_last_deposito = true
            }else{
              this.status_last_deposito = false
            }
          }else{
            this.status_last_deposito = false
          }
        }
      }
    )
  }

  fecharLoad(status :boolean) :void{
    this.estadoLoad = status;
  }

}
