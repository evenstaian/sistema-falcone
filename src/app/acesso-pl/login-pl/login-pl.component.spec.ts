import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPlComponent } from './login-pl.component';

describe('LoginPlComponent', () => {
  let component: LoginPlComponent;
  let fixture: ComponentFixture<LoginPlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
