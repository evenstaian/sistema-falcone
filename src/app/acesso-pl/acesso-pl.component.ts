import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate} from '@angular/animations';

@Component({
  selector: 'app-acesso-pl',
  templateUrl: './acesso-pl.component.html',
  styleUrls: ['./acesso-pl.component.css'],
  animations: [
  
    trigger('animacao-cadastro', [
      state('criado', style({
        opacity: 1
      })),
      transition('void => criado', [
        style({opacity: 0, transform:'translate(0px, -50px)'}),
        animate('500ms 0s ease-in-out')
      ])
    ]),
    
    trigger('animacao-login', [
      state('criado', style({
        opacity: 1
      })),
      transition('void => criado', [
        style({opacity: 0, transform:'translate(0px, -50px)'}),
        animate('500ms 0s ease-in-out')
      ])
    ]),
    trigger('loader', [
      state('frente', style({
        transform: 'rotateY('+ 180 + 'deg)',
      })),
      state('costa', style({
        transform: 'rotateY('+ 360 + 'deg)',
      })),
      transition('frente <=> costa',
        animate('1000ms ease-in-out'))
    ])
  ]
})

export class AcessoPlComponent implements OnInit {

  public estadoPresents :string = 'criado'
  public estadoCadastro :string = 'criado'
  public estadoLogin :string = 'criado'
  public estadoLogo: string = 'frente'

  public cadastro: boolean = false
  public login: boolean = false
  public presents: boolean = true

  public painel: string = "presents"

  constructor() { }

  ngOnInit() {
    
  }

  public exibirPainel(event: string) :void{
    this.painel = event
  }

}

