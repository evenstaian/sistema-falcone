import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  @Output() public exibirPainel: EventEmitter<string> = new EventEmitter()

  public estadoLoad :boolean = false

  constructor() { }

  ngOnInit() {
  }

  public exibirPainelCadastro() :void {
    this.exibirPainel.emit('cadastro')
  }

  public exibirPainelLoginPl() :void {
    this.exibirPainel.emit('login')
  }

}
