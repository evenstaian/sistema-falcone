import { Component, OnInit } from '@angular/core';
import { AcessPagesService } from 'src/services/modules_services/acess_pages.service';

@Component({
  selector: 'app-acess-pages',
  templateUrl: './acess-pages.component.html',
  styleUrls: ['./acess-pages.component.css'],
  providers: [ AcessPagesService ]
})
export class AcessPagesComponent implements OnInit {

  public pages_links : any = AcessPagesService.acess_pages
  public estadoAcesso : boolean = true

  constructor() { }

  ngOnInit() {
  }

}
