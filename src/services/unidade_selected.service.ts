import { Injectable, EventEmitter } from "@angular/core"
import { UnidadeSelected } from "../models/unidade_selected.model"

@Injectable()
export class UnidadeSelectedService {

    unidade_selected :UnidadeSelected

    static titulo_pagina :string

    static emitirUnidadeSelecionada = new EventEmitter<UnidadeSelected>()

    static emitirRefreshUnidades = new EventEmitter<Boolean>()

    enviaUnidadeSelecionada(unidade_selected :UnidadeSelected){
        this.unidade_selected = unidade_selected
        UnidadeSelectedService.emitirUnidadeSelecionada.emit(unidade_selected)
    }

    static putTituloPagina(titulo_pagina){
        this.titulo_pagina = titulo_pagina
    }

    refreshUnidades(refresh :boolean){
        UnidadeSelectedService.emitirRefreshUnidades.emit(refresh);
    }

    getUnidadeSelected(){
        return this.unidade_selected
    }

    getTituloPagina(){
        return UnidadeSelectedService.titulo_pagina
    }

}