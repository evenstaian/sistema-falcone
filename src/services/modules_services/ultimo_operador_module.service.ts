import { Injectable, EventEmitter } from "@angular/core"

@Injectable()
export class UltimoOperadorModuleService {

    static emitirRefreshOperador = new EventEmitter<Boolean>()

    refreshLastOperador(refresh :boolean){
        UltimoOperadorModuleService.emitirRefreshOperador.emit(refresh);
    }

}