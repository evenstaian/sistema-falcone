import { Injectable, EventEmitter } from "@angular/core"

@Injectable()
export class UltimoDepositoModuleService {

    static emitirRefreshUltimoDeposito = new EventEmitter<Boolean>()

    refreshLastDeposito(refresh :boolean){
        UltimoDepositoModuleService.emitirRefreshUltimoDeposito.emit(refresh);
    }

}