import { Injectable, EventEmitter } from "@angular/core"

@Injectable()
export class ErrorInputService {

    static emitirMensagemErro = new EventEmitter<String>()

    static mensagem_erro : string

    enviaMensagemErro(mensagem_erro :string){
        ErrorInputService.mensagem_erro = mensagem_erro
        ErrorInputService.emitirMensagemErro.emit(mensagem_erro)
    }

}