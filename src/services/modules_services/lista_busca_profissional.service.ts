import { Injectable, EventEmitter  } from "@angular/core"

@Injectable()
export class ListaBuscaProfissionalService {

    static profissional : any

    static emitirProfissional = new EventEmitter<any>()

    enviaProfissional(profissional :any){
        ListaBuscaProfissionalService.profissional = profissional
    }

}