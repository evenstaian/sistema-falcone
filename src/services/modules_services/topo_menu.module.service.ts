import { Injectable, EventEmitter  } from "@angular/core"

@Injectable()
export class TopoMenuModuleService {

    static emitirAtivacao = new EventEmitter<Boolean>()
    static emitirEstado = new EventEmitter<string>()
    static emitirEstado1 = new EventEmitter<string>()

    enviaAtivacaoMenu(ativo : boolean, estado: string, estado1: string){
        TopoMenuModuleService.emitirAtivacao.emit(ativo)
        TopoMenuModuleService.emitirEstado.emit(estado)
        TopoMenuModuleService.emitirEstado1.emit(estado1)
    }

}